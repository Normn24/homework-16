"use strict";

// Task 1
// function printArray(array, parent) {
//   for (let i = 0; i < array.length; i++) {
//     let li = document.createElement("li");
//     li.innerText = array[i];
//     parent.append(li);
//   }
// }

// const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

// let ul = document.createElement("ul");
// document.body.append(ul);

// printArray(array, ul);

// Task 2
function printArray(array, parent) {
  const subOl = document.createElement("ol");

  array.map((element) => {
    let li = document.createElement("li");
    if (Array.isArray(element)) {
      element.map((item) => {
        let liNew = document.createElement("li");
        subOl.append(liNew);
        liNew.innerText = item;
      })
      li.innerText = "New array"
      li.append(subOl);
    } else {
      li.innerText = element;
    }

    parent.append(li);
  })
}

const array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

let ul = document.createElement("ul");
document.body.append(ul);

printArray(array, ul);

let finish = 3;
const div = document.createElement("div");
document.body.append(div);

const timer = () => {
  div.innerText = `Очищення сторінки через ${finish} сек.`;
  finish--;

  if (finish < 0) {
    clearInterval(intervalId);
    document.body.innerHTML = "";
  }
};

timer();

const intervalId = setInterval(timer, 1000);